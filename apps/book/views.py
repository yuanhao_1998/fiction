import traceback

from django_redis import get_redis_connection
from rest_framework.response import Response
from rest_framework.views import APIView

from source_code.settings.dev import logger
from utils.pymysql_conn import Conn
from utils.sql_manage import book_query2, bookshelves_query2, select_book_table


class BookReadView(APIView):  # 继续阅读

    def get(self, request):
        book_id = request.query_params.get('book_id')
        chapter_id = request.query_params.get('chapter_id')
        table_name = 'tb_' + str(book_id)
        conn = Conn()
        conn.execute(book_query2, book_id)
        href = conn.fetchone().get('href')
        redis_conn = get_redis_connection('default')
        redis_conn.lpush("book_url", str({"url": href, "table_name": table_name, "book_id": book_id}))
        # noinspection PyBroadException
        try:
            if not chapter_id or chapter_id == '' or chapter_id == 'null':  # 没有阅读记录时读取第一章
                chapter_id = 1

            query = 'SELECT id, chapter_name, content FROM %s' % table_name + ' WHERE id = %s'
            conn.execute(query, chapter_id)
            chapter = conn.fetchone()
        except Exception:
            logger.error(traceback.format_exc())
            return Response({
                'errno': 4001,
                'errmsg': 'database query error'
            })
        # noinspection PyBroadException
        try:
            conn.execute(bookshelves_query2, (chapter_id, chapter.get('chapter_name'), book_id))
            return Response({'errno': 0, 'errmsg': 'OK', **chapter})
        except Exception:
            logger.error(traceback.format_exc())
            conn.rollback()
            return Response({
                'errno': 4007,
                'errmsg': 'database update error'

            })


class BookListAPIView(APIView):  # 返回章节列表

    def get(self, request, book_id):
        conn = Conn()
        table_name = 'tb_' + str(book_id)
        query = select_book_table % table_name
        conn.execute(query)
        data = []
        for value in conn.fetchall():
            data.append(value)
        return Response({
            'errno': 0,
            'errmsg': 'OK',
            'data': data
        })
