from django.db import models

from utils.base_model import BaseModel


class Book(BaseModel):  # 书籍模型类
    book_name = models.CharField(verbose_name='书名', max_length=100)
    author = models.CharField(verbose_name='作者', max_length=50)
    tags = models.CharField(verbose_name='标签', max_length=100, null=True)
    href = models.CharField(verbose_name='URL', max_length=500, default=None)
    is_delete = models.BooleanField(verbose_name='是否删除', default=False)

    class Meta:
        db_table = 'tb_book'

    def __str__(self):
        return self.book_name