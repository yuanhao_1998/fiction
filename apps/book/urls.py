from django.urls import path

from . import views

urlpatterns = [
    path('list/', views.BookListAPIView.as_view()),  # 书籍列表请求
    path('', views.BookReadView.as_view()),  # 阅读请求
    path('list/<int:book_id>/', views.BookListAPIView.as_view()),  # 目录列表请求
]