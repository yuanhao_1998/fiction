import re


def Completion_URL(url):  # 补全URL链接
    if not re.match(r'^http:', url):
        url = 'http://' + url
    return url