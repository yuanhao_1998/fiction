# Fiction_download

* 一个爬取小说的简单网站，可以根据书名搜索，自动从网上爬取内容到数据库并展示，您可以以此构建自己的小说网站。
* 示例网站：`waterberry.cn`
* 推荐部署方法：`nginx + uwsgi`
* fiction_scrapy为爬虫模块，可以通过`scrapy crawl fiction_download`启动
